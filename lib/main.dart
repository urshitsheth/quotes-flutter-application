import 'dart:convert' show jsonDecode;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(new MaterialApp(
      theme: new ThemeData(primarySwatch: Colors.teal),
      debugShowCheckedModeBanner: false,
      home: new Home(),
    ));

class Home extends StatefulWidget {
  @override
  HomeState createState() => new HomeState();
}

class HomeState extends State<Home> {
  List data;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Quotes"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
            onPressed: () {              
              (context as Element).reassemble();
            },
          )
        ],
      ),
      body: new Container(
        child: new Center(
            child: new FutureBuilder(
          future: DefaultAssetBundle.of(context)
              .loadString("load_quotes/quotes.json"),
          builder: (context, snapshot) {
            var quoteList = jsonDecode(snapshot.data.toString());
            if (quoteList.length > 0) {
              quoteList.shuffle();
            }
            return new ListView.builder(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              itemBuilder: (BuildContext context, int index) {
                return new GestureDetector(
                  onTap: () => {
                    Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text("Copy Successfully"))),
                    Clipboard.setData(ClipboardData(
                        text: quoteList[index]['quoteText'] +
                            "-" +
                            quoteList[index]['quoteAuthor']))
                  },
                  child: new Card(
                      child: Padding(
                    padding: EdgeInsets.all(25),
                    child: new ListTile(
                      title: new Text(
                        quoteList[index]['quoteText'],
                        style: new TextStyle(fontSize: 18.0),
                      ),
                      subtitle: new Text(
                        quoteList[index]['quoteAuthor'] == null ||
                                quoteList[index]['quoteAuthor'] == ""
                            ? "- No Author"
                            : "- " + quoteList[index]['quoteAuthor'],
                        style: new TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  )),
                );
              },
              itemCount: quoteList == null ? 0 : 50,
            );
          },
        )),
      ),
    );
  }
}
